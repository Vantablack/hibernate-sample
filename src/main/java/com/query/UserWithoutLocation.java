package com.query;

public class UserWithoutLocation {
    private String name;

    public UserWithoutLocation() {
    }

    public UserWithoutLocation(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
