package com.query;

import com.mapping.manytomany_optimized.User;
import com.shared.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.util.ArrayList;
import java.util.List;

public class QueryExample {
    public static void main(String[] args) {
        QueryExample query = new QueryExample();
        query.dynamicInstantiation();
        HibernateUtil.getSessionFactory().close();
    }

    public void retrieveQuery() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        Query query = session.createQuery("from User");

        List<User> userList = (ArrayList<User>) query.list();

        for (User user : userList) {
            System.out.println(user.toString());
        }

        session.getTransaction().commit();
        session.close();
    }

    /**
     * This uses Implicit Join where by Hibernate automatically determines the SQL query needed to retrieve data
     * Note: Might not be the optimal query so sometimes we have to do an Explicit join
     */
    public void whereQuery() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        Query query = session.createQuery("from User user where user.name = 'Tim'");

        List<User> userList = (ArrayList<User>) query.list();

        for (User user : userList) {
            System.out.println(user.toString());
        }

        session.getTransaction().commit();
        session.close();
    }

    public void pagingQuery() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        Query query = session.createQuery("from User")
                .setFirstResult(4)
                .setMaxResults(5);

        List<User> userList = (ArrayList<User>) query.list();

        for (User user : userList) {
            System.out.println(user.toString());
        }

        session.getTransaction().commit();
        session.close();
    }

    public void deleteQuery() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        Query query = session.createQuery("from User");

        List<User> userList = (ArrayList<User>) query.list();


        User userToDelete = userList.get(0);
        System.out.println("Deleting: ");
        System.out.println(userToDelete.toString());

        session.delete(userToDelete);

        session.getTransaction().commit();
        session.close();
    }

    /**
     * Useful if I just want a subset of the whole table data since I don't have to retrieve, for-loop and parse.
     * Hibernate will do it for me
     */
    public void dynamicInstantiation() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        Query query = session.createQuery("select new com.query.UserWithoutLocation(user.name) from User user");

        List<UserWithoutLocation> userList = (ArrayList<UserWithoutLocation>) query.list();

        for (UserWithoutLocation user : userList) {
            System.out.println("User Name: " + user.getName());
        }

        session.getTransaction().commit();
        session.close();
    }
}
