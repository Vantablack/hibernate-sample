package com.mapping.manytomany_optimized;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class UserLocationId implements Serializable {
    private Integer userId;
    private Integer locationId;

    public UserLocationId() {
    }

    public UserLocationId(Integer userId, Integer locationId) {
        this.userId = userId;
        this.locationId = locationId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getLocationId() {
        return locationId;
    }

    public void setLocationId(Integer locationId) {
        this.locationId = locationId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserLocationId that = (UserLocationId) o;

        if (userId != null ? !userId.equals(that.userId) : that.userId != null) return false;
        return locationId != null ? locationId.equals(that.locationId) : that.locationId == null;

    }

    @Override
    public int hashCode() {
        int result = userId != null ? userId.hashCode() : 0;
        result = 31 * result + (locationId != null ? locationId.hashCode() : 0);
        return result;
    }
}
