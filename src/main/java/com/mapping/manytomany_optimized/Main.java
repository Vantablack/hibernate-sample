package com.mapping.manytomany_optimized;

import com.shared.HibernateUtil;
import org.hibernate.Session;

public class Main {
    public static void main(String[] args) {
        seedData();
    }

    public static void seedData() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        User user = new User("Tom");
        User user1 = new User("Dick");
        User user2 = new User("Harry");
        User user3 = new User("Mary");
        User user4 = new User("John");
        User user5 = new User("Steve");
        User user6 = new User("Tim");

        Location loc = new Location("Hougang");
        Location loc1 = new Location("Kovan");
        Location loc2 = new Location("Serangoon");
        Location loc3 = new Location("Woodleigh");
        Location loc4 = new Location("Potong Pasir");
        Location loc5 = new Location("Farrer Park");

        user.getLocations().add(new UserLocation(user, loc));
        user.getLocations().add(new UserLocation(user, loc1));
        user.getLocations().add(new UserLocation(user, loc2));
        user.getLocations().add(new UserLocation(user, loc3));

        user1.getLocations().add(new UserLocation(user1, loc1));
        user1.getLocations().add(new UserLocation(user1, loc2));

        user2.getLocations().add(new UserLocation(user2, loc2));

        user3.getLocations().add(new UserLocation(user3, loc5));
        user3.getLocations().add(new UserLocation(user3, loc3));

        user4.getLocations().add(new UserLocation(user4, loc4));
        user4.getLocations().add(new UserLocation(user4, loc3));

        user5.getLocations().add(new UserLocation(user5, loc4));
        user5.getLocations().add(new UserLocation(user5, loc5));

        user6.getLocations().add(new UserLocation(user6, loc1));
        user6.getLocations().add(new UserLocation(user6, loc4));

        session.persist(user);
        session.persist(user1);
        session.persist(user2);
        session.persist(user3);
        session.persist(user4);
        session.persist(user5);
        session.persist(user6);

        session.getTransaction().commit();
        session.close();
    }

    public static void nothing() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.close();
        HibernateUtil.getSessionFactory().close();
    }
}
