package com.mapping.manytomany_optimized;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Entity
@Table(name = "user_m_t_m_optimized")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "userId", nullable = false)
    private Integer id;

    @Basic(fetch = FetchType.LAZY)
    @Column(name = "userName", nullable = false, unique = true)
    private String name;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<UserLocation> locations = new ArrayList<>();

    public User() {
    }

    public User(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<UserLocation> getLocations() {
        return locations;
    }

    public void setLocations(List<UserLocation> locations) {
        this.locations = locations;
    }

    public void removeLocation(Location location) {
        for (Iterator<UserLocation> iterator = this.locations.iterator(); iterator.hasNext(); ) {
            UserLocation userLocation = iterator.next();
            if (userLocation.getUser().equals(this) && userLocation.getLocation().equals(location)) {
                iterator.remove();
                userLocation.getLocation().getUsers().remove(userLocation);
                userLocation.setUser(null);
                userLocation.setLocation(null);
                break;
            }
        }
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("User {").append("\n");
        sb.append("\t").append("id=").append(id).append(",\n");
        sb.append("\t").append("name=").append(name).append(",\n");
        sb.append("\t").append("locations= [");
        for (Iterator<UserLocation> iterator = this.locations.iterator(); iterator.hasNext(); ) {
            UserLocation userLocation = iterator.next();
            sb.append("(" + userLocation.getId().getLocationId() + ", " +userLocation.getLocation().getLocationName() +")");
        }
        sb.append("]\n");
        sb.append("}");
        return sb.toString();
    }
}
