package com.mapping.manytomany_optimized;

import javax.persistence.*;

@Entity
@Table(name = "user_location_m_t_m_optimized")
public class UserLocation {

    @EmbeddedId
    private UserLocationId id;

    @ManyToOne
    @MapsId("userId")
    private User user;

    @ManyToOne
    @MapsId("locationId")
    private Location location;

    public UserLocation() {
    }

    public UserLocation(User user, Location location) {
        this.user = user;
        this.location = location;
        this.id = new UserLocationId(user.getId(), location.getId());
    }

    public UserLocationId getId() {
        return id;
    }

    public void setId(UserLocationId id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserLocation that = (UserLocation) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (user != null ? !user.equals(that.user) : that.user != null) return false;
        return location != null ? location.equals(that.location) : that.location == null;

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (user != null ? user.hashCode() : 0);
        result = 31 * result + (location != null ? location.hashCode() : 0);
        return result;
    }
}
