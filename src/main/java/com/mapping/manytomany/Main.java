package com.mapping.manytomany;

import com.shared.HibernateUtil;
import org.hibernate.Session;

public class Main {
    /**
     * Example of Unidirectional @ManyToMany association
     * @param args
     */
    public static void main(String[] args) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        User user = session.get(User.class, 5);

        Location location = new Location();
        location.setId(2);
        location.setLocationName("Redhill");

        user.getLocations().add(location);

        session.save(user);

        session.getTransaction().commit();
        session.close();
        HibernateUtil.getSessionFactory().close();
    }
}
