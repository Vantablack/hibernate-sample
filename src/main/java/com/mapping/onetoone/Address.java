package com.mapping.onetoone;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import javax.persistence.*;

@Entity()
@Table(name = "address_o_t_o")
public class Address {

    @Id
    @Column(name = "userId", unique = true, nullable = false)
    @GeneratedValue(generator = "gen")
    @GenericGenerator(name = "gen", strategy = "foreign", parameters = {@Parameter(name = "property", value = "user")})
    private Integer id;

    @Column(name = "street", nullable = false, length = 255)
    private String street;

    @Column(name = "zipCode", nullable = false, length = 6)
    private String zipCode;

    @Column(name = "country", nullable = false, length = 50)
    private String country;

    @OneToOne
    @PrimaryKeyJoinColumn
    private User user;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
