package com.mapping.onetoone;

import com.shared.HibernateUtil;
import org.hibernate.Session;

public class Main {

    /**
     * One to One Annotation example
     *
     * Somehow this: http://www.journaldev.com/2916/hibernate-one-to-one-mapping-example-annotation
     * is the only one that works
     * @param args
     */
    public static void main(String[] args) {
        Main main = new Main();
        main.addUser();
        HibernateUtil.getSessionFactory().close();
    }

    public void addUser() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        Address address = new Address();
        address.setZipCode("530605");
        address.setCountry("Singapore");
        address.setStreet("Hougang Avenue 4");

        User user = new User("Yaowei");
        user.setAddress(address);
        address.setUser(user);
        session.save(user);

        session.getTransaction().commit();
        session.close();
    }
}
