package com.mapping.onetomany;

import com.shared.HibernateUtil;
import org.hibernate.Session;

public class Main {

    /**
     * Tutorial Links:
     * http://viralpatel.net/blogs/hibernate-one-to-many-annotation-tutorial/
     * https://www.mkyong.com/hibernate/hibernate-one-to-many-relationship-example-annotation/
     * <p>
     * Useful References:
     * https://docs.jboss.org/hibernate/orm/3.6/reference/en-US/html/collections.html
     *
     * @param args
     */
    public static void main(String[] args) {
        Main main = new Main();
        // main.addUser();
        // main.getUserAndUpdate();
        // main.addProject(2, new Project("Best Project"));
        main.addUserAlt();
        HibernateUtil.getSessionFactory().close();
    }

    public void addUser() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        User user = new User();
        user.setName("Test");
        session.save(user);

        Project project = new Project("Project 1");
        project.setUser(user);
        Project project1 = new Project("Project 2");
        project1.setUser(user);
        user.getProjectSet().add(project);
        user.getProjectSet().add(project1);
        session.save(project);
        session.save(project1);

        session.getTransaction().commit();
        session.close();
    }

    public void addUserAlt() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        User user = new User();
        user.setName("Testing 1");
        session.save(user);

        Project project = new Project("Testing Project");
        project.setUser(user);
        Project project1 = new Project("Testing Project 1");
        project1.setUser(user);
        user.addProject(project);
        user.addProject(project1);
        session.save(project);
        session.save(project1);

        session.getTransaction().commit();
        session.close();
    }

    public void addOneUser() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        User user = new User();
        user.setName("Yaowei");
        session.save(user);

        session.getTransaction().commit();
        session.close();
    }

    public void getUser() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        User user = session.get(User.class, 1);
        System.out.println(user.toString());

        session.getTransaction().commit();
        session.close();
    }

    public void getUserAndUpdate() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        User user = session.get(User.class, 1);
        User user1 = session.get(User.class, 2);
        Project project = user.getProjectSet().iterator().next();
        project.setUser(user1);

        System.out.println("BEFORE:");
        System.out.println(user.toString());
        System.out.println("#####");
        System.out.println(user1.toString());

        session.getTransaction().commit();
        session.close();

        System.out.println("AFTER:");
        System.out.println(user.toString());
        System.out.println("#####");
        System.out.println(user1.toString());
    }

    public void addProject(Integer userId, Project project) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        User user = session.get(User.class, userId);
        user.addProject(project);

        session.getTransaction().commit();
        session.close();
    }
}
