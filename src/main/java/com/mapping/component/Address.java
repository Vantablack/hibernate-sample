package com.mapping.component;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class Address {
    @Column(name = "street", nullable = false, length = 255)
    private String street;
    @Column(name = "zipCode", nullable = false, length = 6)
    private String zipCode;
    @Column(name = "country", nullable = false, length = 50)
    private String country;

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Override
    public String toString() {
        return String.format("%s\nStreet: %s, ZipCode: %s, Country: %s", super.toString(), this.street, this.zipCode, this.country);
    }
}
