package com.mapping.component;

import com.shared.HibernateUtil;
import org.hibernate.Session;

public class Main {

    public static void main(String[] args) {
        Main main = new Main();

        Address address = new Address();
        address.setStreet("This is a random street");
        address.setCountry("Singapore");
        address.setZipCode("123456");
        main.addUser("User", address);

        HibernateUtil.getSessionFactory().close();
    }

    public void addUser(String name, Address address) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        User user = new User();
        user.setName(name);
        user.setAddress(address);
        session.save(user);

        session.getTransaction().commit();
        session.close();
    }

    public User getUser(Integer id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        User loadedUser = session.get(User.class, id);
        // User loadedUser = session.load(User.class, 1);

        session.getTransaction().commit();
        session.close();
        return loadedUser;
    }

    public void modifyUser(User user) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        System.out.println("User object before modification: " + user.toString());
        user.setName("Test");
        System.out.println("User object after modification: " + user.toString());

        session.getTransaction().commit();
        session.close();
    }

    public void getUserAndUpdate(Integer id, String newName) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        User loadedUser = session.get(User.class, id);
        System.out.println("User object retrieved: " + loadedUser.toString());
        loadedUser.setName(newName);

        session.getTransaction().commit();
        session.close();
    }
}
