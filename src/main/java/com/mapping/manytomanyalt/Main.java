package com.mapping.manytomanyalt;

import com.shared.HibernateUtil;
import org.hibernate.Session;

public class Main {

    /**
     * Example of Bidirectional @ManyToMany association
     *
     * @param args
     */
    public static void main(String[] args) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        User user = session.get(User.class, 2);

        Location location = new Location();
        location.setLocationName("Kovan");

        user.addLocation(location);
        user.addLocation(session.get(Location.class, 1));

        session.persist(user);

        session.getTransaction().commit();
        session.close();
        HibernateUtil.getSessionFactory().close();
    }
}
