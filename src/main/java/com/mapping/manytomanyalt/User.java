package com.mapping.manytomanyalt;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "user_m_t_m_alt")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "userId", nullable = false)
    private Integer id;

    @Basic(fetch = FetchType.LAZY)
    @Column(name = "userName", nullable = false, unique = true)
    private String name;

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(name = "user_location_m_t_m_alt",
            joinColumns = @JoinColumn(name = "userId"),
            inverseJoinColumns = @JoinColumn(name = "locationId")
    )
    private List<Location> locations = new ArrayList<>();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Location> getLocations() {
        return locations;
    }

    public void setLocations(List<Location> locations) {
        this.locations = locations;
    }

    /**
     * Helper Method to keep both Entities in sync (Bidirectional)
     * @param location
     */
    public void addLocation(Location location) {
        locations.add(location);
        location.getUsers().add(this);
    }

    /**
     * Helper Method to keep both Entities in sync (Bidirectional)
     * @param location
     */
    public void removeLocation(Location location) {
        locations.remove(location);
        location.getUsers().remove(this);
    }

}
